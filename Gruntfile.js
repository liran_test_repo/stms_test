'use strict';

module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);
    var serveStatic = require('serve-static');

    grunt.initConfig({
        watch: {
            js: {
                files: ['app/**/*.js'],
                tasks: [],
                options: {
                    livereload: true
                }
            },
            views: {
                files: ['app/index.html'],
                tasks: [],
                options: {
                    livereload: true
                }
            }
        },
        connect: {
            options: {
                port: 9000,
                base: 'app',
                open: true,
                livereload: 35729,
                hostname: 'localhost',
                keepalive: false
            },
            proxies: [{
                context: ['/api.php'],
                host: 'community-netflix-roulette.p.mashape.com',
                https: true,
                changeOrigin: true
            }],
            livereload: {
                options: {
                    base: ['app'],
                    open: true,
                    middleware: function(connect) {
                        return [
                            require('grunt-connect-proxy/lib/utils').proxyRequest,
                            serveStatic('.tmp'),
                            connect().use(
                                'js/bower',
                                serveStatic('js/bower')
                            ),
                            serveStatic('app')
                        ];
                    }
                }
            }
        }
    });

    grunt.registerTask('serve', [
        'configureProxies',
        'connect:livereload',
        'watch'
    ]);
};
