(function(angular) {
    angular.module('app', [
        'ui.router',
        'ui.bootstrap',
        'datatables'
    ])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('dashboard', {
                abstract: true,
                url: "",
                templateUrl: "netflix/dashboard/dashboardView.html"
            })
            .state('dashboard.provider', {
                url: '/provider',
                controller: 'providerCtrl',
                controllerAs: 'providerCtrl',
                templateUrl: 'netflix/dashboard/provider/providerView.html'
            })
            .state('dashboard.favorites', {
                url: '/favorites', 
                controller: 'favoritesCtrl',
                controllerAs: 'favoritesCtrl',
                templateUrl: 'netflix/dashboard/favorites/favoritesView.html'
            });
            $urlRouterProvider.otherwise('/provider');
    });

})(angular);

