(function(angular) {
    angular.module('app').
    service('favoritesSrvc', [
        function() {
            this.myFavorites = [];

            this.get = function() {
                return this.myFavorites;
            }

            this.update = function(favorite) {
                var foundIndex;
                if (favorite.selected) {
                    foundIndex = _.find(this.myFavorites, { id: favorite.id });
                    if (!foundIndex) {
                        //Add to favorites array
                        this.myFavorites.push(favorite);
                    }
                } else {
                    foundIndex = _.findIndex(this.myFavorites, { id: favorite.id });
                    if (foundIndex) {
                        //Remove from favorites array
                        this.myFavorites.splice(foundIndex, 1);
                    }
                }
                favorite.selected = false;
            }
        }
    ]);
})(angular);
