(function(angular) {
    angular
        .module('app')
        .controller('favoritesCtrl', favoritesCtrl);

    function favoritesCtrl(favoritesSrvc) {
        this.tableData = favoritesSrvc.get();
    }
})(angular);
