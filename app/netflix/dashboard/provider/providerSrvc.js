(function(angular) {
    angular.module('app').
    service('providerSrvc',
        function(favoritesSrvc, $q, network) {
            var self = this;
            this.data = [];
            this.searchText = "";
            this.searchMinLength = 5;
            this.isAllSelected = _.isUndefined(this.isAllSelected) ? false : this.isAllSelected;

            this.getData = function(searchText) {
                var defer = $q.defer();
                if (this.searchText === searchText) {
                    defer.resolve(this.data);
                } else {
                    //New search
                    this.setSearchText(searchText);
                    if (this.searchText.length > this.searchMinLength) {
                        network.api({
                                type: 'getArtistMovies',
                                args: { actor: this.searchText }
                            })
                            .then(function(res) {
                                self.data = res.data;
                                defer.resolve(self.data);
                            }, function(err) {
                                self.data = null;
                                defer.resolve(self.data);
                            })

                    } else {
                        self.data = null;
                        defer.resolve(self.data);
                    }
                }
                return defer.promise;
            }

            this.getSearchText = function() {
                return this.searchText;
            }

            this.setSearchText = function(searchText) {
                this.searchText = angular.copy(searchText);
            }

            this.getIsAllSelected = function() {
                return this.isAllSelected;
            }

            this.setIsAllSelected = function(isAllSelected) {
                this.isAllSelected = angular.copy(isAllSelected);;
            }

            this.updateFavorites = function(favorites) {
                favoritesSrvc.update(angular.copy(favorites));
            }


        }
    );
})(angular);
