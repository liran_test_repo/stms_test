(function(angular) {
    angular
        .module('app')
        .controller('providerCtrl', providerCtrl);

    function providerCtrl(providerSrvc) {
        this.searchText = providerSrvc.getSearchText();
        this.isAllSelected = providerSrvc.getIsAllSelected();
        
        var self = this;

        if (!this.selectedRows) {
            this.selectedRows = [];
        }

        this.getData = function() {
            providerSrvc.getData(this.searchText)
                .then(function(data) {
                    self.tableData = data;
                });
        }

        this.updateFavorites = function(selectedRows, isAllSelected) {
            angular.forEach(selectedRows, function(value) {
                providerSrvc.updateFavorites(value);
            });
            if(!_.isUndefined(isAllSelected)) {
                providerSrvc.setIsAllSelected(isAllSelected);
            }
        }

        this.getData(this.searchText);

    }
})(angular);
