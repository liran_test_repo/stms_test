(function(angular) {
    angular
        .module('app')
        .directive('simpleTable', simpleTable);

    function simpleTable(constants) {
        return {
            restirct: 'E',
            templateUrl: 'netflix/shared/components/simpleTable/simpleTableDrtv.html',
            scope: {
                tableInfoKey: '@',
                rowSelectionArray: '=',
                tableData: '=',
                onSelect: '=',
                enableRowSelection: '=',
                enableComments: '=',
                isAllSelected: '='
            },
            link: function(scope, element, attr) {
                init();

                scope.selectRow = function(row) {
                    if (scope.onSelect) {
                        if (row.selected) {
                            scope.rowSelectionArray.push(row);
                        } else {
                            var rowIndex = _.findIndex(scope.rowSelectionArray, row);
                            if (rowIndex > -1) {
                                scope.rowSelectionArray.splice(rowIndex, 1);
                            }
                        }
                        scope.onSelect([row]);
                    }
                }

                scope.selectAll = function() {
                    angular.forEach(scope.tableData, function(row) {
                        row.selected = scope.table.isAllSelected;
                    });
                    scope.rowSelectionArray = scope.table.isAllSelected ? scope.tableData : [];
                    scope.onSelect(scope.tableData, scope.table.isAllSelected);
                }

                function init() {
                    scope.defaultOrder = constants[scope.tableInfoKey].defaultOrderColumnIndex || 0;
                    scope.defaultDirection = constants[scope.tableInfoKey].defaultOrderColumnDirection || 'desc';
                    scope.dtOptions = {
                        autoWidth: false,
                        paging: true,
                        deferRender: true,
                        searching: true,
                        order: scope.defaultOrder ? [
                            [scope.defaultOrder, scope.defaultDirection]
                        ] : [
                            [0, scope.defaultDirection]
                        ],
                        pageLength: 25
                    };

                    if (scope.enableRowSelection) {
                        scope.dtOptions.aoColumnDefs = [
                            { 'bSortable': false, 'aTargets': [0] }
                        ];
                    }

                    scope.table = {
                        headers: constants[scope.tableInfoKey].tableHeaders,
                        data: scope.tableData,
                        isAllSelected: scope.isAllSelected
                    };
                }

                scope.$watch('tableData', function(newVal, oldVal) {
                    if (newVal !== oldVal) {
                        init();
                    }
                });
            }
        }
    }

})(angular);
