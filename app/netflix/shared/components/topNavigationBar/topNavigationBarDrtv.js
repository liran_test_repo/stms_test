(function(angular) {
    angular
    	.module('app')
    	.directive('topNavigationBar', topNavigationBar);

    function topNavigationBar() {
        return {
            restrict: 'E',
            templateUrl: 'netflix/shared/components/topNavigationBar/topNavigationBarDrtv.html'
        }
    }
})(angular);
