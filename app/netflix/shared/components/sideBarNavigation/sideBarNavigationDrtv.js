(function(angular) {
    angular
        .module('app')
        .directive('sideBarNavigation', sideBarNavigation);

    function sideBarNavigation($state) {
        return {
            restirct: 'E',
            templateUrl: 'netflix/shared/components/sideBarNavigation/sideBarNavigationDrtv.html',
            link: function(scope, element, attr) {
                setActiveState();

                function setActiveState() {
                    scope.activeState = $state.current.name;
                }

                scope.$on('$stateChangeSuccess',
                    function(event, toState, toParams, fromState, fromParams) {
                        setActiveState();
                    });
            }
        }
    }
})(angular);
