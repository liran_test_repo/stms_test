(function(angular) {
    var value = {
        getArtistMovies: {
            method: 'GET',
            url: '/api.php',
            headers: {
                'X-Mashape-Key': 'fgo1jyvj2EmshfdkgLqdiaHv0GXCp1h4z5GjsnbSjIKATmftLX'
            }
        }
    };
    angular.module('app').value('apiMap', value);
})(angular);
