(function(angular) {

    angular
        .module('app')
        .service('network', network);

    function network($http, apiMap) {
        this.api = function(cfg) {
            var request = {
                method: apiMap[cfg.type].method,
                url: apiMap[cfg.type].url,
                headers: apiMap[cfg.type].headers,
                data: cfg.args,
                params: cfg.args
            }
            
            return $http(request);
        };
    }
})(angular);
