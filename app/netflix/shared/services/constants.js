(function (angular) {
    var value = {
        provider: {
            tableHeaders: [
                { value: 'category', displayValue: 'Category' },
                { value: 'show_title', displayValue: 'Title' },
                { value: 'director', displayValue: 'Director'},
                { value: 'rating', displayValue: 'Rating' },
                { value: 'show_cast', displayValue: 'Cast' }
            ],
            defaultOrderColumnIndex: 2
        },
        favorites: {
            tableHeaders: [
                { value: 'category', displayValue: 'Category' },
                { value: 'show_title', displayValue: 'Title' },
                { value: 'director', displayValue: 'Director' },
                { value: 'rating', displayValue: 'Rating' },
                { value: 'show_cast', displayValue: 'Cast' }
            ],
            defaultOrderColumnIndex: 2
        }
    };
    angular.module('app').value('constants', value);
})(angular);
